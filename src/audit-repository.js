var q = require('q');
var _ = require('underscore');
var knex = require('knex');
var errors = require('./errors.js');
var Repository = require('./repository.js');

/* Audit Repository inherits from Repository */
function AuditRepository() {
  Repository.apply(this, arguments);
  this.table = '';
  this.contentTable = '';
  this.views = [];
};
AuditRepository.prototype = Object.create(Repository.prototype);

/* Queryable Fields */
AuditRepository.prototype.getQueryableFields = function(context, view) {
  var base = Repository.prototype.getQueryableFields.apply(this, arguments);
  if(!view || view === this.contentTable) {
    base.push('created_at');
  }
  return base;
};

/* Insertable Fields */
AuditRepository.prototype.getInsertableFields = function(context) {
  var names = _.pluck(this.schema.columns, 'name');
  return _.without(names, 'id', 'revision', 'at');
};

/* Updateable Fields */
AuditRepository.prototype.getUpdateableFields = function(context) {
  var names = _.pluck(this.schema.columns, 'name');
  return _.without(names, 'id', 'revision', 'at');
};

/* Retrieves the main table that records get inserted into or updated in */
AuditRepository.prototype.getMainTable = function() {
  return this.contentTable;
};

/* Returns a default query that selects a current view
 * of the content table */

AuditRepository.prototype.defaultView = function() {

  var query = '(SELECT ' + this.table + '.created_at,' + this.contentTable + '.* FROM ' + this.table
    + ' INNER JOIN ' + this.contentTable
    + ' ON ' + this.table + '.id = ' + this.contentTable + '.id'
    + ' AND ' + this.table + '.revision = ' + this.contentTable + '.revision'
    + ' WHERE ' + this.table + '.deleted_at IS NULL) AS ' + this.table;

  var raw =  this.db.raw(query);
  return raw;
};

/* Finds an object by its id */
AuditRepository.prototype.doFind = function(context, id, view) {
  var table = view;
  if(!view || view == this.table) {
    table = this.defaultView();
  }

  var query = this.db(table);
  if(context.transaction) {
    query.transacting(context.transaction);
  }

  this.defaultFilter(query,view);
  query.where('id', '=', id);

  return query.then(function(results) {
    if(results.length > 0) {
      return results[0];
    } else {
      throw new errors.NotFoundError('could not find object');
    }
  });
};

/* Performs an actual query on the database table or view */
AuditRepository.prototype.doQuery = function(context, query) {
  var q = null;
  query = query || {};
  var view = query.view;

  /* apply the view that is being queried */
  if(view && this.views.indexOf(view) !== -1) {
    q = this.db(view);
  } else {
    view = this.contentTable;
    q = this.db(this.defaultView());
  }

  if(context.transaction) {
    q.transacting(context.transaction);
  }

  /* apply additional clauses */
  this.defaultFilter(context, q, view);

  var queryableFields = this.getQueryableFields(context, view);
  this.applyFilters(q, queryableFields, query.filters || []);
  this.applyOrders(q, queryableFields, query.orderBy || []);
  this.applySkipAndTake(q, query.skip, query.take);

  if(query.count) {
    return q.count('*').then(function(result) {
      return { count : parseInt(result[0].aggregate) };
    })
  } else {
    this.applyFields(q, queryableFields, query.fields);
  }

  return q;
};

/* Performs an actual database insertion */
AuditRepository.prototype.doInsert = function(context, obj) {

  /* transactionally insert rows into
   * both the table and content table */
  var me = this;
  var id = null;

  var go = function(t) {
    return me.db(me.table).transacting(t).insert({ revision : 1, created_at : new Date(), deleted_at : null }, 'id')
      .then(function(newIds) { id = newIds[0]; })
      .then(function() {
        obj.id = id;
        obj.revision = 1;
        obj.at = new Date();
        return me.db(me.contentTable).transacting(t).insert(obj);
    });
  };

  var prom;

  if(context.transaction) {
    prom = go(context.transaction)
      .then(function() { return id });
  } else {
    prom = this.db.transaction(function(t) {
      go(t).then(t.commit, t.rollback);
    });
  }

  return prom.then(function() { return id; });
};

/* Performs an actual database update */
AuditRepository.prototype.doUpdate = function(context, old, obj) {

  /* Transactionally increment current revision
   * and insert the new content object */
  var me = this;
  var revision = null;
  var newObj = _.clone(old);
  _.extend(newObj, obj);
  newObj = _.omit(newObj, ['created_at', 'deleted_at'])
  newObj.at = new Date();

  var go = function(t) {
    return me.db(me.table).transacting(t).where('id', '=', old.id)
      .then(function(recs) {
        revision = recs[0].revision + 1;
        return me.db(me.table).transacting(t).where('id', '=', old.id).update({
          revision : revision
        });
      })
      .then(function() {
        newObj.revision = revision;
        return me.db(me.contentTable).transacting(t).insert(newObj);
      });
  };

  var prom;
  if(context.transaction) {
    prom = go(context.transaction);
  } else {
    prom = this.db.transaction(function(t) {
      go(t).then(t.commit, t.rollback);
    });
  }

  return prom.then(function() { return newObj; });
};


/* Performs the actual deletion of an object */
AuditRepository.prototype.doDelete = function(context, id) {
  /* Just set the deleted_at flag on the table row */

  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  return q
    .where('id', '=', id)
    .update({ deleted_at : new Date() });
};

/* Export the Audit Repository constructor */
module.exports = AuditRepository;