
// simple class that will enable cross origin requests
// for all requests made to the server
var CorsProvider = function() {

};

CorsProvider.prototype.host = function(app) {

  /* Allow Cross Origin Requests to the API */
  app.use(function(req,res,next) {

    if(req.header('Origin')) {
      res.header('Access-Control-Allow-Origin', req.header('Origin'));
    }

    if(req.header('Access-Control-Request-Method')) {
      res.header('Access-Control-Allow-Methods', req.header('Access-Control-Request-Method'));
    }

    if(req.header('Access-Control-Request-Headers')) {
      res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
    }

    next();
  });
};

// export the constructor
module.exports = CorsProvider;