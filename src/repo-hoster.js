var _ = require('underscore');
var q = require('q');

// hosts a repository in an express
// application

var RepositoryHoster = function(errorHandler) {
  this.errorHandler = errorHandler;
};

RepositoryHoster.prototype.onError = function(err, req, res) {
  this.errorHandler.handle(err, req, res);
};

/* Hosts a repository within an express application */
RepositoryHoster.prototype.host = function(app, repo, prefix) {
  var me = this;

  app.get(prefix + 'schema', function(req, res) {
    repo.getSchema(req.context).then(function(schema) {
      res.send(schema, 200);
    }, me.onError.bind(me, req, res));
  });

  /* retrive a single object */
  app.post(prefix + 'find', function(req,res) {
    if(!req.body || !req.body.id) {
      me.onError(req, res, 'id is required');
      return;
    }
    
    var id = req.body.id;
    repo.find(req.context, id, req.body.view).then(function(obj) {
      res.send(obj, 200);
    }, me.onError.bind(me, req, res));
  });

  app.post(prefix + 'all', function(req, res) {
    var view = null;
    if(req.body && req.body.view) {
      view = req.body.view;
    }

    repo.query(req.context, { view : view }).then(function(results) {
      res.send(results, 200);
    }, me.onError.bind(me, req, res));
  });

  /* query for objects */
  app.post(prefix + 'query', function(req, res) {
    if(!req.body) {
      me.onError(req, res, 'json body is required');
      return;
    }

    repo.query(req.context, req.body).then(function(results) {
      res.send(results, 200);
    }, me.onError.bind(me, req, res));
  });

  /* query for first object */
  app.post(prefix + 'first', function(req, res) {
    if(!req.body) {
      me.onError(req, res, 'json body is required');
      return;
    }

    repo.first(req.context, false, req.body).then(function(result) {
      res.send(result, 200);
    }, me.onError.bind(me, req, res));
  });

  /* insert a new object */
  app.post(prefix + 'insert', function(req, res) {
    if(!req.body) {
      me.onError(req, res, 'json body is required');
      return;
    }

    repo.insert(req.context, req.body).then(function(obj) {
      res.send(obj, 200);
    }, me.onError.bind(me, req, res));
  });

  /* updates an existing object */
  app.post(prefix + 'update', function(req, res) {
    if(!req.body) {
      me.onError(req, res, 'json body is required');
      return;
    }

    repo.update(req.context, req.body).then(function(obj) {
      res.send(obj, 200);
    }, me.onError.bind(me, req, res));
  });

  /* deletes an existing object */
  app.post(prefix + 'delete', function(req, res) {
    if(!req.body || !req.body.id) {
      me.onError(req, res, 'id is required');
      return;
    }

    repo.del(req.context, req.body.id).then(function() {
      res.send({}, 200);
    }, me.onError.bind(me, req, res));
  });

  /* performs a changeset on the repository */
  app.post(prefix + 'changeset', function(req, res) {
    if(!req.body) {
      me.onError(req, res, 'json body is required');
      return;
    }

    repo.changeset(req.context, req.body).then(function() {
      res.send({}, 200);
    }, me.onError.bind(me, req, res));
  });

};

// export the constructor 

module.exports = RepositoryHoster;